/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import facade.FileFacade;
import facade.SystuserFacade;
import facade.UserInTeamFacade;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import model.File;
import model.Systuser;
import model.Team;
import model.UserInTeam;

/**
 *
 * @author nigelantwi-boasiako
 */

@Stateless
public class FileService implements Serializable {
    
    @Inject 
    SystuserFacade suFacade;
    
    @Inject
    UserInTeamFacade uit;
    
    @Inject
    FileFacade fiFacade;
    
    public void addFile(String file, String name, int size, String type){
        File f = new File();
        Systuser user = suFacade.find(name);
        f.setSize(size);
        f.setFile(file);
        f.setOwner(user);
        f.setHidden(false);
        f.setAdded(new Date());
        f.setType(type);
        fiFacade.create(f);
        
    }
    
    public void addFileTeam(String file, String name,String team, int size, String type){
        List<UserInTeam>uis;
        Team t = null;
        Systuser s = suFacade.find(name);
        uis = s.getUserInTeamList();
        for(UserInTeam u: uis){
            if(u.getTeam().getTeamname().equals(team)){
                t=u.getTeam();
            }
        }
        File f = new File();
        f.setFile(file);
        f.setTeam(t);
        f.setHidden(false);
        f.setAdded(new Date());
        f.setSize(size);
        f.setType(type);
        fiFacade.create(f);
        
    }
    
}
