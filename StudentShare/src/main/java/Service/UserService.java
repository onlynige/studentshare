/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import facade.SystuserFacade;
import facade.TeamFacade;
import facade.UserInTeamFacade;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import model.Systuser;
import model.Team;
import model.UserInTeam;

/**
 *
 * @author nigelantwi-boasiako
 */


@Stateless
public class UserService implements Serializable {
    
    @Inject 
    SystuserFacade suFacade;
    
    @Inject 
    TeamFacade teamFacade;
    
    @Inject 
    UserInTeamFacade uinFacade;
    
    public boolean findUserLogin(String name, String password) {
        Systuser us = suFacade.find(name);
        if (us != null && us.getPassword().equals(password)) {
            return true;
        }
        return false;
    }
    
    public void register(Systuser user) {
        if (suFacade.find(user.getUsername()) == null) {
            suFacade.create(user);
        }
    }
    
    public void addUserToTeam(Team t, Systuser user){
       UserInTeam ui = new UserInTeam();
       ui.setSystuser(user);
       ui.setTeam(t);
       uinFacade.create(ui);
       
    }
    
    public void deleteTeam(Team t, Systuser user){
        
    }
    
    
    
}
