/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Resources;

import facade.FileFacade;
import facade.SystuserFacade;
import facade.TeamFacade;
import facade.UserInTeamFacade;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.POST;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.File;
import model.Systuser;
import model.Team;
import model.UserInTeam;

/**
 * REST Web Service
 *
 * @author nigelantwi-boasiako
 */
@Path("TeamFile")
@RequestScoped
public class TeamFileResource {

    @Context
    private UriInfo context;

    @Inject
    SystuserFacade suFacade;

    @Inject
    FileFacade fileFacade;

    @Inject
    UserInTeamFacade uitFacade;

    @Inject
    TeamFacade tFacade;

    /**
     * Creates a new instance of TeamFileResource
     */
    public TeamFileResource() {
    }

    @GET
    @Path("list/{id}/{tm}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public List<File> getTeamFiles(@PathParam("id") String id,
            @PathParam("tm") String tm) {
        List<UserInTeam> list;
        List<File> teamfile = new ArrayList<>();
        Team t;
        try {
            System.out.println(id + tm);
            Systuser s = suFacade.find(id);
            System.out.println(s.getUsername());
            list = s.getUserInTeamList();
            System.out.println(tm);
            for (UserInTeam temp : list) {
                if (temp.getTeam().getTeamname().equals(tm)) {
                    System.out.println(temp);
                    t = temp.getTeam();
                    System.out.println(t);
                    teamfile = t.getFileList();
                    System.out.println(teamfile);
                }
            }
            System.out.println(teamfile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return teamfile;
    }

    @POST
    @Path("delete")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteFromTeam(JsonObject obj) {
        HashMap<String, String> out = new HashMap<>();
        try {
            String team = obj.getString("b");
            String fil = obj.getString("file");
            String user = obj.getString("user");
            String output = "";
            Team t = null;
            List<File> teamfile = fileFacade.findAll();
            Systuser s = suFacade.find(user);
            List<UserInTeam> list = s.getUserInTeamList();

            for (UserInTeam temp : list) {
                if (temp.getTeam().getTeamname().equals(team)) {
                    // System.out.println(temp);
                    t = temp.getTeam();
                     //System.out.println(t);
                }
            }
            
            for (File f : teamfile) {
                System.out.println("fffff");
                System.out.println(t);
                if (f.getFile().equals(fil) && f.getTeam() == t) {
                    System.out.println(f);
                    output = "returned";
                    fileFacade.remove(f);
                    out.put("Status", output);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok()
                .entity(out.get("Status")).build();
    }

}
