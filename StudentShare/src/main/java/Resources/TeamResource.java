/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Resources;

import facade.SystuserFacade;
import facade.TeamFacade;
import facade.UserInTeamFacade;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.POST;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.File;
import model.Systuser;
import model.Team;
import model.UserInTeam;

/**
 * REST Web Service
 *
 * @author nigelantwi-boasiako
 */
@Path("team")
@RequestScoped
public class TeamResource {

    @Context
    private UriInfo context;

    @Inject
    TeamFacade teamFacade;

    @Inject
    SystuserFacade suFacade;

    @Inject
    UserInTeamFacade uitFacade;

    /**
     * Creates a new instance of TeamResource
     */
    public TeamResource() {
    }

    @POST
    @Path("createteam")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createTeam(JsonObject obj) {
        String name = obj.getString("team");
        String user = obj.getString("user");
        Systuser s = suFacade.find(user);
        String output = "";
        try {
            List<Team> teams = teamFacade.findWithName(name);
            List<Team> usersTeam = new ArrayList<>();

            for (Team temp : teams) {
                if (temp.getTeamname().equals(name) && temp.getCreatedby().equals(user)) {
                    usersTeam.add(temp);
                }
            }
            if (teams.isEmpty()) {
                Team t = new Team();
                t.setTeamname(name);
                t.setCreatedby(user);
                teamFacade.create(t);
                UserInTeam uitt = new UserInTeam();
                uitt.setSystuser(s);
                uitt.setTeam(t);
                System.out.println(s);
                uitFacade.create(uitt);
                //adduser into team
                output = "team created";
            } else {
                output = "Team already exists";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok().entity(output).build();
    }

    @POST
    @Path("adduser")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addUserToTeam(JsonObject obj) {
        Systuser first = suFacade.find(obj.getString("usr1"));
        Systuser second = suFacade.find(obj.getString("usr2"));
        Systuser cr = suFacade.find(obj.getString("creator"));
        String name = obj.getString("team");
        String creator = obj.getString("creator");
        String output = "";
        //create a check to see whether or not users in team already
        try {
            List<Team> teams = teamFacade.findAll();
            for (Team temp : teams) {
                if (temp.getTeamname().equals(name) && temp.getCreatedby().equals(creator)) {
                    Team t = temp;
                    UserInTeam uit = new UserInTeam();
                    uit.setSystuser(first);
                    uit.setTeam(t);
                    UserInTeam ui = new UserInTeam();
                    ui.setSystuser(second);
                    ui.setTeam(t);
                    uitFacade.create(uit);
                    uitFacade.create(ui);
                    output = "Users added";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok().entity(output).build();
    }

    @GET
    @Path("list/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public List<UserInTeam> getUsersTeam(@PathParam("id") String id) {
        List<UserInTeam> list = new ArrayList<>();
        try {
            
            Systuser s = suFacade.find(id);
            System.out.println(s);
            list = s.getUserInTeamList();
            System.out.println("i");
            System.out.println(s.getUserInTeamList()); //DEBUG no need!!!
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    
    @GET
    @Path("list/users/{id}/{team}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public List<UserInTeam> getUsersInTeam(@PathParam("id") String id,
            @PathParam("team") String team) {
        List<UserInTeam> list = new ArrayList<>();
        UserInTeam utt = null;
        try {
            
            Systuser s = suFacade.find(id);
            List<UserInTeam>ut = uitFacade.findAll();
            
            for(UserInTeam u: ut){
                if(u.getSystuser().equals(s) && u.getTeam().getTeamname().equals(team)){
                    utt = u;
                }
            }
             for(UserInTeam n: ut ){
                 if(n.getTeam()==utt.getTeam()){
                     list.add(n);
                 }
             }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    
    @POST
    @Path("deleteuser")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteTeamMember(JsonObject obj){
        String output = "";
      try{
        Systuser current = suFacade.find(obj.getString("current"));
        String team = obj.getString("id");
        List<UserInTeam>uit = uitFacade.findAll();
        UserInTeam t = null;
        for(UserInTeam temp: uit){
            if(temp.getSystuser().equals(current) && temp.getTeam().getTeamname().equals(team)){
                t=temp;
                uitFacade.remove(t);
                output = "User removed";
            }
        }
        //for(UserInTeam n : uit){
         //   if(n == t &&n.getSystuser()==current){
         //       uitFacade.remove(n);
          //      output = "User removed";
          //  }
       // }
      }catch(Exception e){
          e.printStackTrace();
      }
        return Response.ok().entity(output).build();
    }
    
    
    
}
