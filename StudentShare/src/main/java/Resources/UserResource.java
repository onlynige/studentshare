/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Resources;

import facade.FileFacade;
import facade.SystuserFacade;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.POST;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.File;
import model.Systuser;

/**
 * REST Web Service
 *
 * @author nigelantwi-boasiako
 */
@Path("user")
@RequestScoped
public class UserResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of UserResource
     */
    public UserResource() {
    }

    @Inject
    SystuserFacade systFacade;

    @Inject
    FileFacade fileFacade;

    @GET
    @Path("list/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public List<File> getUsersFileList(@PathParam("id") String id) {
        List<File> list = new ArrayList<>();
        try {
            Systuser s = systFacade.find(id);
            System.out.println(s.getUsername());
            List<File> files = fileFacade.findAll();

            for (File temp : files) {
                if (s.getFileList().contains(temp) && temp.getHidden()==false) {
                    System.out.println(temp.getFile());
                    list.add(temp);
                }
            }
            System.out.println(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    
    

    @POST
    @Path("share")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response shareFile(JsonObject obj) {
        HashMap<String, String> out = new HashMap<>();
        try {
            String user = obj.getString("user");
            System.out.println(user);
            String file = obj.getString("id");
            System.out.println("hhh");
            System.out.println(file);
            String owner = obj.getString("current");
            System.out.println(owner);

            File fil = new File();
            String output = "";
            List<File> files = fileFacade.findAll();
            Systuser currentowner = systFacade.find(owner);
            Systuser use = systFacade.find(user);
            if (use != null && currentowner != null) {
                for (File f : files) {
                    if (f.getFile().equals(file) && f.getOwner().equals(currentowner)) {
                        fil.setFile(file);
                        System.out.println(file);
                        fil.setOwner(use);
                        fileFacade.create(fil);
                        output = "File shared with" + use.getUsername();
                        out.put("Status", output);
                    } //
                }
            } else {
                output = "User cannot be found";
            }
            out.put("Status", output);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok()
                .entity(out.get("Status")).build();
    }

    @POST
    @Path("delete")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteFile(JsonObject obj) {
        HashMap<String, String> out = new HashMap<>();
        String output = "";
        try {
            String name = obj.getString("name");
            String file = obj.getString("file");
            System.out.println(name);
            System.out.println(file);
            Systuser user = systFacade.find(name);
            List<File> files = fileFacade.findFile(file);
            for (File temp : files) {
                if (user.getFileList().contains(temp)) {
                    temp.setHidden(Boolean.TRUE);
                    fileFacade.edit(temp);
                    output = "deleted";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok()
                .entity(out.get("Status")).build();

    }
    
    @GET
    @Path("list/deleted/{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public List<File> getUsersDelFileList(@PathParam("id") String id) {
        List<File> list = new ArrayList<>();
        try {
            Systuser s = systFacade.find(id);
            System.out.println(s.getUsername());
            List<File> files = fileFacade.findAll();

            for (File temp : files) {
                if (s.getFileList().contains(temp) && temp.getHidden()==true) {
                    System.out.println(temp.getFile());
                    list.add(temp);
                }
            }
            System.out.println(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

}
