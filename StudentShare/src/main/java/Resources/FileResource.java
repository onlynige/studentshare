/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Resources;

import Service.FileService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

/**
 * REST Web Service
 *
 * @author nigelantwi-boasiako
 */
@Path("file")
@RequestScoped
public class FileResource {

    @Context
    private UriInfo context;

    @Inject
    FileService fls;

    /**
     * Creates a new instance of FileResource
     */
    public FileResource() {
    }

    @POST
    @Path("upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFile(
            @FormDataParam("session") String s,
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail) {
        String fileLocation = "/Users/nigelantwi-boasiako/Documents/ProjectTest/" + fileDetail.getFileName();

        try {
            File f = new File(fileLocation);
            FileOutputStream out = new FileOutputStream(f);
            int read = 0;
            byte[] bytes = new byte[1024];
            out = new FileOutputStream(f);
            while ((read = uploadedInputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            
            System.out.println(s);
            fls.addFile(fileDetail.getFileName(), s, fileDetail.hashCode(),fileDetail.getType());
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String output = "File successfully uploaded to : " + fileLocation;
        return Response.status(200).entity(output).build();

    }

    @GET
    @Path("txt/{s}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getFile(@PathParam("s") String s) {
         return download(s);
    }

    private Response download(String fileName) {
        String FILE_PATH = "/Users/nigelantwi-boasiako/Documents/ProjectTest/";
        String fileLocation = FILE_PATH + fileName;
        Response response = null;
        File file = new File(FILE_PATH + fileName);
        if (file.exists()) {
            System.out.println("exist");
            ResponseBuilder builder = Response.ok(file);
            builder.header("Content-Disposition", "attachment; filename=" + file.getName());
            System.out.println(file.getName());
            response = builder.build();
            System.out.println(response);
            long file_size = file.length();
        } else {
            System.out.println("no");
            response = Response.status(404).
                    entity("FILE NOT FOUND: " + fileLocation).
                    type("text/plain").
                    build();
        }
        return response;
    }

    @POST
    @Path("uploadteam")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadTeamFile(
            @FormDataParam("session") String s,
            @FormDataParam("team") String team,
            @FormDataParam("file") InputStream uploadedInputStream,
            @FormDataParam("file") FormDataContentDisposition fileDetail) {
        String fileLocation = "/Users/nigelantwi-boasiako/Documents/ProjectTest/" + fileDetail.getFileName();

        try {
            System.out.println("dddd");
            File f = new File(fileLocation);
            FileOutputStream out = new FileOutputStream(f);
            int read = 0;
            byte[] bytes = new byte[1024];
            out = new FileOutputStream(f);
            while ((read = uploadedInputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            System.out.println(fileDetail.getFileName());
            fls.addFileTeam(fileDetail.getFileName(), s, team,fileDetail.hashCode(),fileDetail.getType());
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String output = "File successfully uploaded to : " + fileLocation;
        return Response.status(200).entity(output).build();

    }
    
    @GET
    @Path("txt/team/{s}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getTeamFile(@PathParam("s") String s) {
         return downloadTeam(s);
    }

    private Response downloadTeam(String fileName) {
        String FILE_PATH = "/Users/nigelantwi-boasiako/Documents/ProjectTest/";
        String fileLocation = FILE_PATH + fileName;
        Response response = null;
        File file = new File(FILE_PATH + fileName);
        if (file.exists()) {
            System.out.println("exist");
            ResponseBuilder builder = Response.ok(file);
            builder.header("Content-Disposition", "attachment; filename=" + file.getName());
            System.out.println(file.getName());
            response = builder.build();
            System.out.println(response);
            long file_size = file.length();
        } else {
            System.out.println("no");
            response = Response.status(404).
                    entity("FILE NOT FOUND: " + fileLocation).
                    type("text/plain").
                    build();
        }
        return response;
    }
}
