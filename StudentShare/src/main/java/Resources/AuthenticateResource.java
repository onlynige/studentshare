/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Resources;

import Security.Hash;
import Service.UserService;
import facade.SystuserFacade;
import java.util.HashMap;
import javax.inject.Inject;
import javax.json.JsonObject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import model.Systuser;

/**
 * REST Web Service
 *
 * @author nigelantwi-boasiako
 */
@Path("authenticate")
public class AuthenticateResource {

    @Context
    private UriInfo context;
    
    @Inject 
    UserService service;
    
    @Inject 
    SystuserFacade systFacade;
    
    @Inject
    Hash dig;
    
    /**
     * Creates a new instance of AuthenticateResource
     */
    public AuthenticateResource() {
    }
    
    
    /**
     * User signs up by providing their name, username,email and password which 
     * is then checked in the data based. Returns "User name already exist" if username has already be taken
     * and "ok" if account has been created.
     * @param user is JsonObect which holds an array of string containing name,email,username,password.
     * @return a response message 
     */
    @POST
    @Path("signupto")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response signup(JsonObject user) {
        HashMap<String, String> out = new HashMap<>();
        try {
            String output = "";
            if (service.findUserLogin(user.getString("username"), 
                    user.getString("password")) == true) {
                output = "Username already exists";
                out.put("Status", output);
            } else {
                Systuser usern = new Systuser();
                usern.setName(user.getString("name"));
                usern.setEmail(user.getString("email"));
                usern.setUsername(user.getString("username"));
                String nh = dig.hash(user.getString("password"));
                System.out.println(nh);
                usern.setPassword(nh);
                service.register(usern);
                output = "ok";
                out.put("Status", output);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok().entity(out.get("Status")).build();
    }
    
    /**
     * User logs in by providing both username and password, which is then 
     * checked in the database. 
     * @param obj
     * @return 
     */
    @POST
    @Path("login")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response login(JsonObject obj) {
        HashMap<String, String> out = new HashMap<>();
        try {
            String name = obj.getString("useremail");
            String password = dig.hash(obj.getString("password"));

            boolean found = service.findUserLogin(name, password);
            String output = "";
            if (found) {
                output = "ok";
                out.put("Status", output);
            } else {
                output = "not found";
                out.put("Status", output);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok()
                .entity(out.get("Status")).build();
    }

    /**
     * 
     * @param obj
     * @return 
     */
    public Response changePassword(JsonObject obj) {
        HashMap<String, String> out = new HashMap<>();
        try {
            String name = obj.getString("useremail");
            String password = dig.hash(obj.getString("password"));
            String newpassword = dig.hash(obj.getString("newpass"));

            boolean found = service.findUserLogin(name, password);
            String output = "";
            if (found) {
                output = "ok";
                Systuser u = systFacade.find(name);
                u.setPassword(password);
                systFacade.edit(u);
                out.put("Status", output);
            } else {
                output = "incorrect password";
                out.put("Status", output);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.ok()
                .entity(out.get("Status")).build();
    }
}
