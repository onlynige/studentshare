/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.File;

/**
 *
 * @author nigelantwi-boasiako
 */
@Stateless
public class FileFacade extends AbstractFacade<File> {
    @PersistenceContext(unitName = "com.FYP_StudentShare_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FileFacade() {
        super(File.class);
    }
    
    public List findFile(String name) {
        return getEntityManager().createQuery(
                "SELECT f FROM File f WHERE f.file = :file")
                .setParameter("file", name)
                .setMaxResults(10)
                .getResultList();
    }
    
}
