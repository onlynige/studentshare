/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.Team;

/**
 *
 * @author nigelantwi-boasiako
 */
@Stateless
public class TeamFacade extends AbstractFacade<Team> {

    @PersistenceContext(unitName = "com.FYP_StudentShare_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TeamFacade() {
        super(Team.class);
    }

    public List findWithName(String name) {
        return getEntityManager().createQuery(
                "SELECT t FROM Team t WHERE t.teamname = :teamname")
                .setParameter("teamname", name)
                .setMaxResults(10)
                .getResultList();
    }

}
