/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.Folder;

/**
 *
 * @author nigelantwi-boasiako
 */
@Stateless
public class FolderFacade extends AbstractFacade<Folder> {
    @PersistenceContext(unitName = "com.FYP_StudentShare_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FolderFacade() {
        super(Folder.class);
    }
    
}
