/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author nigelantwi-boasiako
 */
public class Hash {
    
    public String hash(String input) throws NoSuchAlgorithmException{
        String salt = "iAMMert3HAILssWERbxjd";
        String newInput = salt+input;
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(newInput.getBytes());
        
        byte bData[] = md.digest();
        StringBuffer hexString = new StringBuffer();
    	for (int i=0;i<bData.length;i++) {
    		String hex=Integer.toHexString(0xff & bData[i]);
   	     	if(hex.length()==1) hexString.append('0');
   	     	hexString.append(hex);
    	}
    	return hexString.toString();
    }
}
