/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author nigelantwi-boasiako
 */
@Entity
@Table(name = "FOLDER", catalog = "", schema = "NIGEL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Folder.findAll", query = "SELECT f FROM Folder f"),
    @NamedQuery(name = "Folder.findById", query = "SELECT f FROM Folder f WHERE f.id = :id"),
    @NamedQuery(name = "Folder.findByName", query = "SELECT f FROM Folder f WHERE f.name = :name")})
public class Folder implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Size(max = 255)
    @Column(name = "NAME", length = 255)
    private String name;
    @JoinColumn(name = "FILE", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
    private File file;
    @OneToMany(mappedBy = "parent", fetch = FetchType.EAGER)
    private List<Folder> folderList;
    @JoinColumn(name = "PARENT", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
    private Folder parent;
    @JoinColumn(name = "OWNER", referencedColumnName = "USERNAME")
    @ManyToOne(fetch = FetchType.EAGER)
    private Systuser owner;
    @JoinColumn(name = "TEAM", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
    private Team team;

    public Folder() {
    }

    public Folder(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    @XmlTransient
    public List<Folder> getFolderList() {
        return folderList;
    }

    public void setFolderList(List<Folder> folderList) {
        this.folderList = folderList;
    }

    public Folder getParent() {
        return parent;
    }

    public void setParent(Folder parent) {
        this.parent = parent;
    }

    public Systuser getOwner() {
        return owner;
    }

    public void setOwner(Systuser owner) {
        this.owner = owner;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Folder)) {
            return false;
        }
        Folder other = (Folder) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Folder[ id=" + id + " ]";
    }
    
}
