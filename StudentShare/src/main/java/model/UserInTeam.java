/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nigelantwi-boasiako
 */
@Entity
@Table(name = "USER_IN_TEAM", catalog = "", schema = "NIGEL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserInTeam.findAll", query = "SELECT u FROM UserInTeam u"),
    @NamedQuery(name = "UserInTeam.findById", query = "SELECT u FROM UserInTeam u WHERE u.id = :id")})
public class UserInTeam implements Serializable {
    @JoinColumn(name = "SYSTUSER", referencedColumnName = "USERNAME")
    @ManyToOne(fetch = FetchType.EAGER)
    private Systuser systuser;
    @JoinColumn(name = "TEAM", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
    private Team team;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;

    public UserInTeam() {
    }

    public UserInTeam(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserInTeam)) {
            return false;
        }
        UserInTeam other = (UserInTeam) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.UserInTeam[ id=" + id + " ]";
    }

    public Systuser getSystuser() {
        return systuser;
    }

    public void setSystuser(Systuser systuser) {
        this.systuser = systuser;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }
    
}
