/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author nigelantwi-boasiako
 */
@Entity
@Table(name = "SYSTUSER", catalog = "", schema = "NIGEL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Systuser.findAll", query = "SELECT s FROM Systuser s"),
    @NamedQuery(name = "Systuser.findByName", query = "SELECT s FROM Systuser s WHERE s.name = :name"),
    @NamedQuery(name = "Systuser.findByEmail", query = "SELECT s FROM Systuser s WHERE s.email = :email"),
    @NamedQuery(name = "Systuser.findByUsername", query = "SELECT s FROM Systuser s WHERE s.username = :username"),
    @NamedQuery(name = "Systuser.findByPassword", query = "SELECT s FROM Systuser s WHERE s.password = :password")})
public class Systuser implements Serializable {
    @OneToMany(mappedBy = "systuser", fetch = FetchType.EAGER)
    private List<UserInTeam> userInTeamList;
    private static final long serialVersionUID = 1L;
    @Size(max = 255)
    @Column(name = "NAME", length = 255)
    private String name;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 255)
    @Column(name = "EMAIL", length = 255)
    private String email;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "USERNAME", nullable = false, length = 20)
    private String username;
    @Size(max = 255)
    @Column(name = "PASSWORD", length = 255)
    private String password;
    @OneToMany(mappedBy = "owner", fetch = FetchType.EAGER)
    private List<File> fileList;
    @OneToMany(mappedBy = "owner", fetch = FetchType.EAGER)
    private List<Folder> folderList;

    public Systuser() {
    }

    public Systuser(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @XmlTransient
    public List<File> getFileList() {
        return fileList;
    }

    public void setFileList(List<File> fileList) {
        this.fileList = fileList;
    }

    @XmlTransient
    public List<Folder> getFolderList() {
        return folderList;
    }

    public void setFolderList(List<Folder> folderList) {
        this.folderList = folderList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (username != null ? username.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Systuser)) {
            return false;
        }
        Systuser other = (Systuser) object;
        if ((this.username == null && other.username != null) || (this.username != null && !this.username.equals(other.username))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Systuser[ username=" + username + " ]";
    }

    @XmlTransient
    public List<UserInTeam> getUserInTeamList() {
        return userInTeamList;
    }

    public void setUserInTeamList(List<UserInTeam> userInTeamList) {
        this.userInTeamList = userInTeamList;
    }
    
}
